package biz.shahed.freelance.heidi.mos.sencha.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Service;

import biz.shahed.freelance.heidi.mos.sencha.SenchaData;
import biz.shahed.freelance.heidi.mos.sencha.SenchaSimpleData;
import biz.shahed.freelance.heidi.mos.sencha.data.MetaData;
import biz.shahed.freelance.heidi.mos.sencha.data.SenchaDataFieldProcessor;
import biz.shahed.freelance.heidi.mos.sencha.data.SenchaField;
import biz.shahed.freelance.heidi.mos.sencha.util.MetaDataUtil;


@Service
public class SenchaDataServiceImpl<K> implements SenchaDataService<K> {
	
	@Override
	public Serializable getIdProperty(Class<?> clazz, K data){
		final BeanWrapper wrapper = new BeanWrapperImpl(data);
		String propertyName = this.getIdPropertyName(clazz, data);
		Object propertyValue = wrapper.getPropertyValue(propertyName);
		return (Serializable)propertyValue;
	}
	
	@Override
	public Serializable getIdProperty(K data){
		Class<?> clazz = data.getClass();
		return this.getIdProperty(clazz, data);
	}
	
	@Override
	public String getIdPropertyName(Class<?> clazz, K data){
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(clazz,  data);
		return processor.getIdProperty();
	}
	
	@Override
	public SenchaData<K> getSenchaData(Class<?> clazz, K data){
		MetaData metaData = MetaDataUtil.getMetaData(clazz, data);
		return new SenchaSimpleData<K>(metaData, data);
	}
	
	@Override
	public SenchaData<K> getSenchaData(Class<?> clazz, K data, long total){
		MetaData metaData = MetaDataUtil.getMetaData(clazz, data);
		return new SenchaSimpleData<K>(metaData, data, total);
	}
	
	@Override
	public SenchaData<K> getSenchaData(Class<?> clazz, K data, long total, boolean success){
		MetaData metaData = MetaDataUtil.getMetaData(clazz, data);
		return new SenchaSimpleData<K>(metaData, data, total, success);
	}
	
	@Override
	public SenchaData<K> getSenchaData(Class<?> clazz, K data, long total, boolean success, String message){
		MetaData metaData = MetaDataUtil.getMetaData(clazz, data);
		return new SenchaSimpleData<K>(metaData, data, total, success, message);
	}
	
	@Override
	public SenchaData<K> getSenchaData(Class<?> clazz, K data, String root, long total, boolean success, String message){
		MetaData metaData = MetaDataUtil.getMetaData(clazz, data, root);
		return new SenchaSimpleData<K>(metaData, data, total, success, message);
	}
	
	@Override
	public SenchaData<K> getSenchaData(List<SenchaField> fields, K data, long total){
		MetaData metaData = MetaDataUtil.getMetaData(fields);
		return new SenchaSimpleData<K>(metaData, data, total);
	}
	
	@Override
	public SenchaData<K> getSenchaData(List<SenchaField> fields, K data, long total, boolean success){
		MetaData metaData = MetaDataUtil.getMetaData(fields);
		return new SenchaSimpleData<K>(metaData, data, total, success);
	}
	
	@Override
	public SenchaData<K> getSenchaData(List<SenchaField> fields, K data, long total, boolean success, String message){
		MetaData metaData = MetaDataUtil.getMetaData(fields);
		return new SenchaSimpleData<K>(metaData, data, total, success, message);
	}
	
	@Override
	public SenchaData<K> getSenchaData(MetaData metaData, K data){
		return new SenchaSimpleData<K>(metaData, data);
	}
	
	@Override
	public SenchaData<K> getSenchaData(MetaData metaData, K data, long total){
		return new SenchaSimpleData<K>(metaData, data, total);
	}
	
	@Override
	public SenchaData<K> getSenchaData(MetaData metaData, K data, long total, boolean success){
		return new SenchaSimpleData<K>(metaData, data, total, success);
	}
	
	@Override
	public SenchaData<K> getSenchaData(MetaData metaData, K data, long total, boolean success, String message){
		return new SenchaSimpleData<K>(metaData, data, total, success, message);
	}

}
