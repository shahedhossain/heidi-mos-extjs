package biz.shahed.freelance.heidi.mos.sencha;

import biz.shahed.freelance.heidi.mos.sencha.data.MetaData;

public class SenchaSimpleData<K> implements SenchaData<K> {

	private MetaData metaData;
	private boolean success;
	private String message;
	private long total;
	private K data;

	public SenchaSimpleData() {
		this(null, null);
	}

	public SenchaSimpleData(MetaData metaData, K data) {
		this(metaData, data, DEFAULT_TOTAL);
	}

	public SenchaSimpleData(MetaData metaData, K data, long total) {
		this(metaData, data, total, DEFAULT_SUCCESS);
	}

	public SenchaSimpleData(MetaData metaData, K data, long total, boolean success) {
		this(metaData, data, total, success, null);
	}

	public SenchaSimpleData(MetaData metaData, K data, long total, boolean success, String message) {
		this.message = resolveMessage(success, message);
		this.metaData = metaData;
		this.success = success;
		this.total = total;
		this.data = data;
	}

	public MetaData getMetaData() {
		return metaData;
	}

	public void setMetaData(MetaData metaData) {
		this.metaData = metaData;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public K getData() {
		return data;
	}

	public void setData(K data) {
		this.data = data;
	}

	private String resolveMessage(boolean success, String message) {
		if (message == null || message.isEmpty()) {
			message = success ? SUCCESS_MESSAGE : FAILURE_MESSAGE;
		}
		return message;
	}

}
