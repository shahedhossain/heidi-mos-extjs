package biz.shahed.freelance.heidi.mos.sencha.data;

import java.util.ArrayList;
import java.util.List;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.util.PatternUtil;

public class SenchaSimpleField implements SenchaField{
	
	private static final Logger log = LoggerFactory.getLogger(SenchaSimpleField.class);
	
	private String name;
	private String mapping;
	private String type;
	
	public SenchaSimpleField(String name){
		this(name, STRING, name);
	}
	
	public SenchaSimpleField(String name, String type){
		this(name, type, name);
	}
	
	public SenchaSimpleField(String name, String type, String mapping){
		this(name, type, mapping, null);
	}
	
	public SenchaSimpleField(String name, String type, String mapping, Object data){
		this.mapping = resolveMapping(name, mapping, data);
		this.assertProperty(name);
		this.assertType(type);
		this.type    = type;
		this.name    = name;		
	}	
		
	private void assertProperty(String name){
		boolean isValidProperty = PatternUtil.isMatch(name, VALD_PROPERTY_EXP);
		if(!isValidProperty){
			try {
				String format  = "%s is not a valid field name";
				String message = String.format(format, name);
				throw new Exception(message);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}

	private void assertType(String type){
		if(!isValidType(type)){
			try {
				String message = "%s is not a valid Extjs type!";
				throw new Exception(String.format(message, type));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}

	private Object getFirstRecord(Object data){
		Object record = null;	
		if(data != null){
			if(data instanceof List<?> || data instanceof ArrayList<?>){
				List<?> records = (List<?>) data;
				if(!records.isEmpty()){
					record= records.get(FIRST_RECORD_INDEX);
				}
			}else {
				record = data;
			} 			
		}
		return record;			
	}

	@Override
	public String getMapping() {
		return mapping;
	}

	@Override
	public String getName() {
		return name;
	}
		
	@Override
	public String getType() {
		return type;
	}

	private boolean hasProperty(String mapping, Object data){
		boolean hasProperty = false;
		try {
			if(isValidMapping(mapping) || isValidSimpleMapping(mapping)){
				MVEL.eval(mapping, getFirstRecord(data));
				hasProperty = true;
			}
		}catch(Exception e){
			log.debug(e.getMessage(), e);
		}
		return hasProperty;
	}

	private boolean isValidMapping(String mapping){
		return PatternUtil.isMatch(mapping, MAPPING_EXP);
	}
	
	private boolean isValidSimpleMapping(String mapping){
		return PatternUtil.isMatch(mapping, SIMPLE_MAPPING_EXP);
	}
		
	private boolean isValidType(String type){
		return PatternUtil.isMatch(type, VAID_DATA_TYPE_EXP);
	}
	
	private String resolveMapping(String name, String mapping, Object data){
		if(!mapping.isEmpty() && !name.equals(mapping)){
			mapping = hasProperty(mapping, data) ? mapping : name;
		}
		return mapping;
	}
		
	protected void setMapping(String mapping) {
		this.mapping = mapping;
	}
	
	protected void setName(String name) {
		this.name = name;
	}
		
	protected void setType(String type) {
		this.type = type;
	}
	
}
