package biz.shahed.freelance.heidi.mos.sencha.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SenchaDateField extends SenchaSimpleField {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(SenchaDateField.class);

	private String dateFormat;

	public SenchaDateField(String name) {
		super(name);
	}

	public SenchaDateField(String name, String type) {
		super(name, type);
	}

	public SenchaDateField(String name, String type, String mapping) {
		super(name, type, mapping);
	}

	public SenchaDateField(String name, String type, String mapping, Object data) {
		super(name, type, mapping, data);
	}

	public String getDateFormat() {
		return dateFormat;
	}

	// TODO SortableDateTime: "Y-m-d\\TH:i:s"
	protected void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

}
