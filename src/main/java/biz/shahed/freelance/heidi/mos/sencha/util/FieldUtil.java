package biz.shahed.freelance.heidi.mos.sencha.util;

import biz.shahed.freelance.heidi.mos.sencha.data.SenchaDateField;
import biz.shahed.freelance.heidi.mos.sencha.data.SenchaField;
import biz.shahed.freelance.heidi.mos.sencha.data.SenchaSimpleField;

public class FieldUtil {
	
	public static SenchaField getField(String name){
		return new SenchaSimpleField(name);
	}
	
	public static SenchaField getField(String name, String type){
		if(type.equalsIgnoreCase(SenchaField.DATE)){
			return new SenchaDateField(name, type);
		}
		return new SenchaSimpleField(name, type);
	}
	
	public static SenchaField getField(String name, String type, String mapping){
		if(type.equalsIgnoreCase(SenchaField.DATE)){
			return new SenchaDateField(name, type, mapping);
		}
		return new SenchaSimpleField(name, type, mapping);
	}
	
	public static SenchaField getField(String name, String type, String mapping, Object data){
		if(type.equalsIgnoreCase(SenchaField.DATE)){
			return new SenchaDateField(name, type, mapping, data);
		}
		return new SenchaSimpleField(name, type, mapping, data);
	}
			
}
