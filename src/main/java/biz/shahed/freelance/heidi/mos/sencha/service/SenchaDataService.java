package biz.shahed.freelance.heidi.mos.sencha.service;

import java.io.Serializable;
import java.util.List;

import biz.shahed.freelance.heidi.mos.sencha.SenchaData;
import biz.shahed.freelance.heidi.mos.sencha.data.MetaData;
import biz.shahed.freelance.heidi.mos.sencha.data.SenchaField;


public interface SenchaDataService<K> {

	Serializable getIdProperty(Class<?> clazz, K data);

	Serializable getIdProperty(K data);

	String getIdPropertyName(Class<?> clazz, K data);

	SenchaData<K> getSenchaData(Class<?> clazz, K data);

	SenchaData<K> getSenchaData(Class<?> clazz, K data, long total);

	SenchaData<K> getSenchaData(Class<?> clazz, K data, long total, boolean success);

	SenchaData<K> getSenchaData(Class<?> clazz, K data, long total, boolean success, String message);

	SenchaData<K> getSenchaData(Class<?> clazz, K data, String root, long total, boolean success, String message);

	SenchaData<K> getSenchaData(List<SenchaField> fields, K data, long total);

	SenchaData<K> getSenchaData(List<SenchaField> fields, K data, long total, boolean success);

	SenchaData<K> getSenchaData(List<SenchaField> fields, K data, long total, boolean success, String message);

	SenchaData<K> getSenchaData(MetaData metaData, K data);
	
	SenchaData<K> getSenchaData(MetaData metaData, K data, long total);
		
	SenchaData<K> getSenchaData(MetaData metaData, K data, long total, boolean success);
	
	SenchaData<K> getSenchaData(MetaData metaData, K data, long total,boolean success, String message);
	
}
