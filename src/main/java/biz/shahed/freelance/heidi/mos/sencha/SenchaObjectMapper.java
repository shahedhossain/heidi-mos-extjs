package biz.shahed.freelance.heidi.mos.sencha;

import java.text.SimpleDateFormat;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.DeserializerProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.map.SerializerProvider;

public class SenchaObjectMapper extends ObjectMapper {
	
	public static final String SENCHA_DATE_FORMATE = "yyyy-MM-dd'T'HH:mm:ss";

	public SenchaObjectMapper() {
		super();
        configure(Feature.WRITE_DATES_AS_TIMESTAMPS, false);
        setDateFormat(new SimpleDateFormat(SENCHA_DATE_FORMATE));
	}

	public SenchaObjectMapper(JsonFactory jf) {
		super(jf);
		configure(Feature.WRITE_DATES_AS_TIMESTAMPS, false);
        setDateFormat(new SimpleDateFormat(SENCHA_DATE_FORMATE));
	}

	public SenchaObjectMapper(JsonFactory jf, SerializerProvider sp, DeserializerProvider dp) {
		super(jf, sp, dp);
		configure(Feature.WRITE_DATES_AS_TIMESTAMPS, false);
        setDateFormat(new SimpleDateFormat(SENCHA_DATE_FORMATE));
	}

	public SenchaObjectMapper(JsonFactory jf, SerializerProvider sp, DeserializerProvider dp, SerializationConfig sconfig, DeserializationConfig dconfig) {
		super(jf, sp, dp, sconfig, dconfig);
		configure(Feature.WRITE_DATES_AS_TIMESTAMPS, false);
        setDateFormat(new SimpleDateFormat(SENCHA_DATE_FORMATE));
	}

}
