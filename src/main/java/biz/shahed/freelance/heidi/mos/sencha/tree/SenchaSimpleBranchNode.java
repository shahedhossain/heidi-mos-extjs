package biz.shahed.freelance.heidi.mos.sencha.tree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SenchaSimpleBranchNode implements Serializable, SenchaTreeNode {

	private static final long serialVersionUID = 4438550885564689345L;
	
	private String id;
	private String text;
	private String iconCls;
	private boolean expanded;
	private List<SenchaTreeNode> children = new ArrayList<SenchaTreeNode>(0);
	
	public SenchaSimpleBranchNode(String text){
		this(null, text);
	}
	
	public SenchaSimpleBranchNode(String id, String text){
		this(id, text, null);
	}
	
	public SenchaSimpleBranchNode(String id, String text, String iconCls){
		this(id, text, iconCls, false);
	}	
	
	public SenchaSimpleBranchNode(String id, String text, String iconCls, boolean expanded){
		this(id, text, iconCls, expanded, new ArrayList<SenchaTreeNode>());
	}
	
	public SenchaSimpleBranchNode(String id, String text, String iconCls, boolean expanded, List<SenchaTreeNode> children){
		this.id = id;
		this.text = text;
		this.iconCls = iconCls;
		this.expanded = expanded;
		this.children = children;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public List<SenchaTreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<SenchaTreeNode> children) {
		this.children = children;
	}

}
