package biz.shahed.freelance.heidi.mos.sencha.data;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.sencha.util.FieldUtil;
import biz.shahed.freelance.heidi.mos.util.PatternUtil;
import biz.shahed.freelance.heidi.mos.util.TypeUtil;


public class SenchaDataFieldProcessor {

	private static final Logger log = LoggerFactory.getLogger(SenchaDataFieldProcessor.class);
	
	public static final String DOT_OPERATOR       = ".";
	public static final String MAPPING_TPL        = "%s.%s";
	
	private Field[] fields;
	private Class<?> clazz;
	private Object data;	

	public SenchaDataFieldProcessor(Class<?> clazz) {
		this(clazz, null);
	}
	
	public SenchaDataFieldProcessor(Class<?> clazz, Object data) {
		this.fields = clazz.getDeclaredFields();
		this.clazz  = clazz;
		this.data   = data;
	}
	
	private SenchaDataField getDataField(Field field) {
		for (Annotation annotation : field.getAnnotations()) {
			if (annotation instanceof SenchaDataField) {
				return (SenchaDataField) annotation;
			}
		}
		return null;
	}
	
	private String getDataType(Field field) {

		if (TypeUtil.isJavaType(field)) {
			if (TypeUtil.isInt(field)) {
				return SenchaField.INTEGER;
			} else if (TypeUtil.isDate(field)) {
				return SenchaField.DATE;
			} else if (TypeUtil.isFloat(field)) {
				return SenchaField.FLOAT;
			} else if (TypeUtil.isBoolean(field)) {
				return SenchaField.BOOLEAN;
			}else if (TypeUtil.isString(field)) {
				return SenchaField.STRING;
			} 
		}else if (TypeUtil.isString(field)) {
			return SenchaField.STRING;
		}

		return SenchaField.AUTO;
	}
	
	public List<SenchaField> getField(){
		List<SenchaField> fields = new ArrayList<SenchaField>();
		for(Field field: this.fields){
			SenchaField dataField = getField(field);
			if(dataField != null){
				fields.add(dataField);
			}
		}
		return fields;
	}
	
	private SenchaField getField(Field field){		
		if(hasSenchaDataFieldAnnotation(field)){
			SenchaDataField dataField = getDataField(field);
			String type    = resolveType(field, dataField);
			String name    = resolveName(field, dataField);
			String mapping = resolveMapping(field, dataField);
			return FieldUtil.getField(name, type, mapping, this.data);			
		}
		return null;
	}
	
	public SenchaField getIdField(){
		
		for(Field field: this.fields){
			if(hasIdProperty(field)){
				return getIdField(field);
			}
		}
		
		try {
			String message = "%s class does not contians id field!";
			throw new Exception(String.format(message, this.clazz.getCanonicalName()));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		return null;
	}
	
	private SenchaField getIdField(Field field){		
		if(hasIdProperty(field)){
			SenchaDataField dataField = getDataField(field);
			String type    = resolveType(field, dataField);
			String name    = resolveName(field, dataField);
			String mapping = resolveMapping(field, dataField);
			return FieldUtil.getField(name, type, mapping, this.data);	
		}
		return null;
	}
	
	public String getIdProperty(){
		SenchaField senchaField = getIdField();
		return senchaField.getName();
	}
	
	private String getReference(String mapping){
		String reference = null;
		if(isValidMapping(mapping)){
			reference = mapping.split(SenchaSimpleField.DOT_OPERATOR)[SenchaSimpleField.REFERENCE_INDEX];
		}
		return reference;
	}

	private String getReferenceField(String mapping){
		String reference = null;
		if(isValidMapping(mapping)){
			reference = mapping.split(SenchaSimpleField.DOT_OPERATOR)[SenchaSimpleField.REFERENCE_FIELD_INDEX];
		}
		return reference;
	}
	
	private boolean hasSenchaDataFieldAnnotation(Field field) {
		for (Annotation annotation : field.getAnnotations()) {
			return annotation instanceof SenchaDataField;
		}
		return false;
	}
	
	public boolean hasIdProperty(){
		for(Field field: this.fields){
			if(hasIdProperty(field)){
				return true;
			}
		}
		return false;
	}
	
	private boolean hasIdProperty(Field field){		
		if(hasSenchaDataFieldAnnotation(field)){
			SenchaDataField dataField = getDataField(field);
			if(dataField.idProperty()){
				return true;
			}			
		}
		return false;
	}
	
	private boolean isSenchaType(Field field){
		return TypeUtil.isJavaType(field) || TypeUtil.isString(field);
	}
	
	private boolean isValidMapping(String mapping){
		return PatternUtil.isMatch(mapping, SenchaField.MAPPING_EXP);
	}
	
	private String resolveMapping(Field field, SenchaDataField dataField){
		String mapping = dataField.mapping();
		if(mapping.isEmpty()){
			String reference      = resolveReference(field, dataField);
			String referenceField = resolveReferenceField(field, dataField);
			if(!isSenchaType(field) && !referenceField.isEmpty()){
				mapping = String.format(MAPPING_TPL, reference, referenceField);
			}else{
				mapping = field.getName();
			}
		}
		return mapping;
	}
	
	private String resolveName(Field field, SenchaDataField dataField){
		return !dataField.name().isEmpty() ? dataField.name() : field.getName();
	}
	
	private String resolveReference(Field field, SenchaDataField dataField){
		String reference = field.getName();
		String mapping   = dataField.mapping();
		
		if(!dataField.reference().isEmpty()){
			reference = dataField.reference();
		}else if(isValidMapping(mapping)){
			reference = getReference(mapping);
		}
		
		return reference;		
	}
	
	//TODO
	private String resolveReferenceField(Field field, SenchaDataField dataField){
		String referenceField = "";
		String mapping = dataField.mapping();
		
		if(!dataField.referenceField().isEmpty()){
			referenceField = dataField.referenceField();
		}else if(isValidMapping(mapping)){
			referenceField = getReferenceField(mapping);
		}
		
		return referenceField;
	}
	
	private String resolveType(Field field, SenchaDataField dataField){
		return dataField.type().equals(SenchaField.AUTO) ? getDataType(field) : dataField.type();
	}

}
