package biz.shahed.freelance.heidi.mos.sencha.tree;

import java.io.Serializable;

public class SenchaSimpleLeafNode implements Serializable, SenchaTreeNode {

	private static final long serialVersionUID = 7249968192390922062L;

	private String id;
	private String text;
	private boolean leaf = true;
	private String iconCls;
	
	public SenchaSimpleLeafNode(String text){
		this(null, text);
	}
	
	public SenchaSimpleLeafNode(String id, String text){
		this(id, text, null);
	}
	
	public SenchaSimpleLeafNode(String id, String text, String iconCls){
		this.id = id;
		this.text = text;
		this.iconCls = iconCls;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isLeaf() {
		return leaf;
	}

	protected void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

}
