package biz.shahed.freelance.heidi.mos.sencha.data;

import java.util.ArrayList;
import java.util.List;

public class SimpleMetaData implements MetaData {

	private boolean implicitIncludes;
	private String  messageProperty;
	private String  successProperty;
	private String  totalProperty;
	private String  idProperty;
	private List<SenchaField> fields;
	private String  root;	
	
	
	public SimpleMetaData(){
		this(DEFAULT_ROOT_PROPERTY, DEFAULT_ID_PROPERTY);		
	}
	
	public SimpleMetaData(List<SenchaField> fields){
		this(fields, DEFAULT_ID_PROPERTY);
	}
	
	public SimpleMetaData(List<SenchaField> fields, String idProperty){
		this(DEFAULT_ROOT_PROPERTY, idProperty, fields);
	}	
	
	public SimpleMetaData(String idProperty){
		this(DEFAULT_ROOT_PROPERTY, idProperty);
	}
	
	public SimpleMetaData(String root, String idProperty){
		this(root, idProperty, new ArrayList<SenchaField>());	
	}
	
	public SimpleMetaData(String root, String idProperty, List<SenchaField> fields){		
		this.implicitIncludes = DEFAULT_IMPLICIT_INCLUDES;
		this.messageProperty  = DEFAULT_MESSAGE_PROPERTY;
		this.successProperty  = DEFAULT_SUCCESS_PROPERTY;
		this.totalProperty    = DEFAULT_TOTAL_PROPERTY;		
		this.idProperty       = idProperty;
		this.fields			  = fields;
		this.root			  = root;		
	}

	@Override
	public String getMessageProperty() {
		return messageProperty;
	}

	protected void setMessageProperty(String messageProperty) {
		this.messageProperty = messageProperty;
	}

	@Override
	public String getTotalProperty() {
		return totalProperty;
	}

	protected void setTotalProperty(String totalProperty) {
		this.totalProperty = totalProperty;
	}

	@Override
	public String getSuccessProperty() {
		return successProperty;
	}

	protected void setSuccessProperty(String successProperty) {
		this.successProperty = successProperty;
	}

	@Override
	public boolean getImplicitIncludes() {
		return implicitIncludes;
	}

	protected void setImplicitIncludes(boolean implicitIncludes) {
		this.implicitIncludes = implicitIncludes;
	}

	@Override
	public String getRoot() {
		return root;
	}

	protected void setRoot(String root) {
		this.root = root;
	}
		
	@Override
	public List<SenchaField> getFields() {
		return fields;
	}

	protected void setFields(List<SenchaField> fields) {
		this.fields = fields;
	}

	@Override
	public String getIdProperty() {
		return idProperty;
	}

	protected void setIdProperty(String idProperty) {
		this.idProperty = idProperty;
	}

}
