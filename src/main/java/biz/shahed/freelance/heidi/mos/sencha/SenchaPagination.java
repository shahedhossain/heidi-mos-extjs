package biz.shahed.freelance.heidi.mos.sencha;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.util.JsonUtil;

public class SenchaPagination implements Serializable {

	private static final long serialVersionUID = 5654762437453476470L;
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(SenchaPagination.class);
		
	private int page;
	private int limit;
	private int start;

	private String dir;
	private String sort;
	private String filter;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}
	
	@SuppressWarnings("unchecked")
	public List<SenchaFilter> getSenchaFilters() {
		String filter = getFilter();
		List<SenchaFilter> filters = new ArrayList<SenchaFilter>();
		if (filter != null && !filter.isEmpty()) {
			filters = (List<SenchaFilter>) JsonUtil.stringAsObject(filter,
					new TypeReference<ArrayList<SenchaFilter>>() {
					});
		}
		return filters;
	}

	public Map<String, Object> getFilters() {
		Map<String, Object> map = new HashMap<String, Object>();
		List<SenchaFilter> filters = getSenchaFilters();
		if (filters != null && !filters.isEmpty()) {
			for (SenchaFilter filter : filters) {
				map.put(filter.getProperty(), filter.getValue());
			}
		}
		return map;
	}
	
}
