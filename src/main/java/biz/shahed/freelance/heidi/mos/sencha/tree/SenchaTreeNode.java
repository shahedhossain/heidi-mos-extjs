package biz.shahed.freelance.heidi.mos.sencha.tree;

public interface SenchaTreeNode {
	
	String getId();

	String getText();

	String getIconCls();

}
