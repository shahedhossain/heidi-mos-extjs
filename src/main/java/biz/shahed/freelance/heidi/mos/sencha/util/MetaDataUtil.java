package biz.shahed.freelance.heidi.mos.sencha.util;

import java.util.List;

import biz.shahed.freelance.heidi.mos.sencha.data.MetaData;
import biz.shahed.freelance.heidi.mos.sencha.data.SenchaDataFieldProcessor;
import biz.shahed.freelance.heidi.mos.sencha.data.SenchaField;
import biz.shahed.freelance.heidi.mos.sencha.data.SimpleMetaData;

public class MetaDataUtil {
		
	public static MetaData getMetaData(){
		return new SimpleMetaData();
	}
	
	public static MetaData getMetaData(Class<?> clazz){
		return getMetaData(clazz, MetaData.DEFAULT_ROOT_PROPERTY);
	}
	
	public static MetaData getMetaData(Class<?> clazz, Object data){
		return getMetaData(clazz, data, MetaData.DEFAULT_ROOT_PROPERTY);
	}
	
	public static MetaData getMetaData(Class<?> clazz, Object data, String root){
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(clazz,  data);
		String idProperty  = processor.getIdProperty();
		List<SenchaField> fields = processor.getField();
		return getMetaData(root, idProperty, fields);
	}
	
	public static MetaData getMetaData(Class<?> clazz, String root){
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(clazz);
		String idProperty  = processor.getIdProperty();
		List<SenchaField> fields = processor.getField();
		return getMetaData(root, idProperty, fields);
	}
	
	public static MetaData getMetaData(List<SenchaField> fields){
		return new SimpleMetaData(fields);
	}
	
	public static MetaData getMetaData(List<SenchaField> fields, String idProperty){
		return new SimpleMetaData(fields, idProperty);
	}
	
	public static MetaData getMetaData(String idProperty){
		return new SimpleMetaData(idProperty);
	}
	
	public static MetaData getMetaData(String root, String idProperty){
		return new SimpleMetaData(root, idProperty);
	}
	
	public static MetaData getMetaData(String root, String idProperty, List<SenchaField> fields){
		return new SimpleMetaData(root, idProperty, fields);
	}

}
