package biz.shahed.freelance.heidi.mos.sencha;

import biz.shahed.freelance.heidi.mos.sencha.data.MetaData;


public interface SenchaData<K> {
	
	public long DEFAULT_TOTAL = 0;
	public boolean DEFAULT_SUCCESS = true;
	public String  SUCCESS_MESSAGE = "Query executed Successfully";
	public String  FAILURE_MESSAGE = "Query failure!";

	public MetaData getMetaData();

	public K getData();

	public boolean isSuccess();

	public String getMessage();

	public long getTotal();

}
