package biz.shahed.freelance.heidi.mos.sencha.data;

public interface SenchaField {

	public int FIRST_RECORD_INDEX     = 0;

	public String DOT_OPERATOR        = ".";
	public String MAPPING_TPL         = "%s.%s";
	public String MAPPING_EXP         = "[a-z]{1}[\\w]*+.[a-z]{1}[\\w]*+";
	public String SIMPLE_MAPPING_EXP  = "[a-z]{1}[\\w]*+";
	public String VAID_DATA_TYPE_EXP  = "auto|int|date|float|string|boolean";
	public String VALD_PROPERTY_EXP   = "[a-z]{1}[\\w&&[^\\_]]*+";

	public int REFERENCE_INDEX        = 0;
	public int REFERENCE_FIELD_INDEX  = 1;
	
	public String AUTO    = "auto";
	public String INTEGER = "int";
	public String DATE    = "date";
	public String FLOAT   = "float";
	public String STRING  = "string";
	public String BOOLEAN = "boolean";

	public String getName();

	public String getMapping();

	public String getType();

}
