package biz.shahed.freelance.heidi.mos.sencha.data;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Payload;

import biz.shahed.freelance.heidi.mos.sencha.data.SenchaField;

@Documented
@Target(value = { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface SenchaDataField {
	
	String type() default SenchaField.AUTO;
	
	String name() default "";
	
	String mapping() default "";
	
	String reference() default "";
	
	boolean idProperty() default false;
	
	String referenceField() default "";	

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
