package biz.shahed.freelance.heidi.mos.sencha.data;

import java.util.List;

public interface MetaData {

	String DEFAULT_MESSAGE_PROPERTY   = "message";
	String DEFAULT_SUCCESS_PROPERTY   = "success";	
	String DEFAULT_TOTAL_PROPERTY     = "total";	
	String DEFAULT_ROOT_PROPERTY      = "data";
	String DEFAULT_ID_PROPERTY        = "id";
	boolean DEFAULT_IMPLICIT_INCLUDES = true;
	
	String getMessageProperty();

	String getTotalProperty();

	String getSuccessProperty() ;

	boolean getImplicitIncludes();

	String getRoot();
	
	List<SenchaField> getFields();

	String getIdProperty();

}
