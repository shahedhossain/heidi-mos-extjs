package biz.shahed.freelance.heidi.mos.sencha.data;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.sencha.data.SenchaDataFieldProcessor;
import biz.shahed.freelance.heidi.mos.sencha.data.SenchaField;

public class SenchaDataFieldProcessorTest extends TestCase  {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(SenchaDataFieldProcessorTest.class);

	public SenchaDataFieldProcessorTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(SenchaDataFieldProcessorTest.class);
	}
		
	public void testBoolean() {		
		D00I000 data = new D00I000();
		data.setNoJavaType(new D00I000());
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(D00I000.class, data);
		for(SenchaField field: processor.getField()){
			if(field.getName().equals("wrapperBoolean")){
				assertTrue(field.getMapping().equals("wrapperBoolean"));
				assertFalse(field.getMapping().equals("wrapperBooleanx"));
			}else if(field.getName().equals("primitiveBoolean")){
				assertTrue(field.getMapping().equals("primitiveBoolean"));
				assertFalse(field.getMapping().equals("primitiveBooleanx"));
			}
		}
	}

	public void testByte() {	
		D00I000 data = new D00I000();
		data.setNoJavaType(new D00I000());
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(D00I000.class, data);
		for(SenchaField field: processor.getField()){
			if(field.getName().equals("wrapperByte")){
				assertTrue(field.getMapping().equals("wrapperByte"));
				assertFalse(field.getMapping().equals("wrapperBytex"));
			}else if(field.getName().equals("primitiveByte")){
				assertTrue(field.getMapping().equals("primitiveByte"));
				assertFalse(field.getMapping().equals("primitiveBytex"));
			}
		}
	}
	
	public void testShort() {
		D00I000 data = new D00I000();
		data.setNoJavaType(new D00I000());
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(D00I000.class, data);
		for(SenchaField field: processor.getField()){
			if(field.getName().equals("wrapperShort")){
				assertTrue(field.getMapping().equals("wrapperShort"));
				assertFalse(field.getMapping().equals("wrapperShortx"));
			}else if(field.getName().equals("primitiveShort")){
				assertTrue(field.getMapping().equals("primitiveShort"));
				assertFalse(field.getMapping().equals("primitiveShortx"));
			}
		}
	}

	public void testChar() {
		D00I000 data = new D00I000();
		data.setNoJavaType(new D00I000());
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(D00I000.class, data);
		for(SenchaField field: processor.getField()){
			if(field.getName().equals("wrapperChar")){
				assertTrue(field.getMapping().equals("wrapperChar"));
				assertFalse(field.getMapping().equals("wrapperCharx"));
			}else if(field.getName().equals("primitiveChar")){
				assertTrue(field.getMapping().equals("primitiveChar"));
				assertFalse(field.getMapping().equals("primitiveCharx"));
			}
		}
	}

	public void testInt() {
		D00I000 data = new D00I000();
		data.setNoJavaType(new D00I000());
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(D00I000.class, data);
		for(SenchaField field: processor.getField()){
			if(field.getName().equals("wrapperInt")){
				assertTrue(field.getMapping().equals("wrapperInt"));
				assertFalse(field.getMapping().equals("wrapperIntx"));
			}else if(field.getName().equals("primitiveInt")){
				assertTrue(field.getMapping().equals("primitiveInt"));
				assertFalse(field.getMapping().equals("primitiveIntx"));
			}
		}
	}

	public void testLong() {
		D00I000 data = new D00I000();
		data.setNoJavaType(new D00I000());
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(D00I000.class, data);
		for(SenchaField field: processor.getField()){
			if(field.getName().equals("wrapperLong")){
				assertTrue(field.getMapping().equals("wrapperLong"));
				assertFalse(field.getMapping().equals("wrapperLongx"));
			}else if(field.getName().equals("primitiveLong")){
				assertTrue(field.getMapping().equals("primitiveLong"));
				assertFalse(field.getMapping().equals("primitiveLongx"));
			}
		}
	}

	public void testFloat() {
		D00I000 data = new D00I000();
		data.setNoJavaType(new D00I000());
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(D00I000.class, data);
		for(SenchaField field: processor.getField()){
			if(field.getName().equals("wrapperFloat")){
				assertTrue(field.getMapping().equals("wrapperFloat"));
				assertFalse(field.getMapping().equals("wrapperFloatx"));
			}else if(field.getName().equals("primitivefloat")){
				assertTrue(field.getMapping().equals("primitivefloat"));
				assertFalse(field.getMapping().equals("primitivefloatx"));
			}
		}
	}

	public void testDouble() {
		D00I000 data = new D00I000();
		data.setNoJavaType(new D00I000());
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(D00I000.class, data);
		for(SenchaField field: processor.getField()){
			if(field.getName().equals("wrapperDouble")){
				assertTrue(field.getMapping().equals("wrapperDouble"));
				assertFalse(field.getMapping().equals("wrapperDoublex"));
			}else if(field.getName().equals("primitiveDouble")){
				assertTrue(field.getMapping().equals("primitiveDouble"));
				assertFalse(field.getMapping().equals("primitiveDoublex"));
			}
		}
	}
	
	public void testString() {		
		D00I000 data = new D00I000();
		data.setNoJavaType(new D00I000());
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(D00I000.class, data);
		for(SenchaField field: processor.getField()){
			if(field.getName().equals("stringType")){
				assertTrue(field.getMapping().equals("stringType"));
				assertFalse(field.getMapping().equals("stringTypex"));
			}
		}
	}
	
	public void testNoJavaType() {
		D00I000 data = new D00I000();
		data.setNoJavaType(new D00I000());
		SenchaDataFieldProcessor processor = new SenchaDataFieldProcessor(D00I000.class, data);
		for(SenchaField field: processor.getField()){
			if(field.getName().equals("idx")){
				assertTrue(field.getMapping().equals("noJavaType"));
				assertFalse(field.getMapping().equals("noJavaTypex"));
			}
		}
	}
}
