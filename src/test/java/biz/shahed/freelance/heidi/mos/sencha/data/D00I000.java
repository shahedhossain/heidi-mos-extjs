package biz.shahed.freelance.heidi.mos.sencha.data;

import biz.shahed.freelance.heidi.mos.sencha.data.SenchaDataField;
import biz.shahed.freelance.heidi.mos.sencha.data.SenchaField;


public class D00I000 {
	
	@SenchaDataField
	public Boolean wrapperBoolean;
	
	@SenchaDataField(type=SenchaField.BOOLEAN)
	public boolean primitiveBoolean;
	
	@SenchaDataField
	public Byte wrapperByte;
	
	@SenchaDataField
	public byte primitiveByte;
	
	
	@SenchaDataField
	public Short wrapperShort;
	
	@SenchaDataField
	public short primitiveShort;
	
	@SenchaDataField
	public Character wrapperChar;
	
	@SenchaDataField
	public char primitiveChar;
	
	@SenchaDataField
	public Integer wrapperInt;
	
	@SenchaDataField
	public int primitiveInt;	
	
	@SenchaDataField
	public Long wrapperLong;
	
	@SenchaDataField
	public long primitiveLong;	
	
	@SenchaDataField
	public Float wrapperFloat;
	
	@SenchaDataField
	public float primitivefloat;	
	
	@SenchaDataField
	public Double wrapperDouble;
	
	@SenchaDataField
	public double primitiveDouble;
	
	@SenchaDataField(name="styep")
	public String stringType;
	
	@SenchaDataField(name="idx", type=SenchaField.INTEGER)
	public D00I000 noJavaType;

	public D00I000 getNoJavaType() {
		return noJavaType;
	}

	public void setNoJavaType(D00I000 noJavaType) {
		this.noJavaType = noJavaType;
	}

}