package biz.shahed.freelance.heidi.mos.sencha;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SenchaDataTest extends TestCase {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(SenchaDataTest.class);

	public SenchaDataTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(SenchaDataTest.class);
	}
	
	public void testFirst() {
//		SenchaData<D00I000> data = new SenchaDataImpl<D00I000>();
		String expected = "Hello Bangladesh!";
		String actual  	= "Hello Bangladesh!";
		assertEquals(expected, actual);
//		log.info("OK");
	}

}
