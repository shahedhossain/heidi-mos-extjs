package biz.shahed.freelance.heidi.mos.sencha.data;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.sencha.data.MetaData;
import biz.shahed.freelance.heidi.mos.sencha.data.SimpleMetaData;

public class MetaDataTest extends TestCase  {

	private static final Logger log = LoggerFactory.getLogger(MetaDataTest.class);

	public MetaDataTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(MetaDataTest.class);
	}
	
	public void testPropertyAccess(){
		MetaData meta= new SimpleMetaData();
		try{
			MVEL.eval("idProperty",meta);
			MVEL.getProperty("idProperty", null);
		}catch(Exception e){
			log.debug(e.getMessage(), e);
		}
		assertEquals(MVEL.eval("idProperty",meta), "id");
	}
}
